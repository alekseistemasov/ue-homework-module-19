// UEHomeworkMod19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Animal {
public:
    virtual void Voice() {
        std::cout << "voice";
    }
};

class Dog : public Animal {
public:
    void Voice() override {
        std::cout << "Woof\n";
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        std::cout << "Mew\n";
    }
};

class Goose : public Animal {
public:
    void Voice() override {
        std::cout << "GaGa\n";
    }
};



int main()
{
    Dog dog;
    Cat cat;
    Goose goose;

    Animal* animals[3] = {&dog, &cat, &goose};
    for (Animal* animal : animals) {
        animal->Voice();
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
